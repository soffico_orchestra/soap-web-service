# base Command line instructions for GitLab
<p>
<details>
<summary>Git global setup</summary>
These details <em>will</em> remain <strong>hidden</strong> until expanded.
<pre><code>
git config --global user.name "your username"
git config --global user.email "your email"
</code></pre>
</details>
</p>

<p>
<details>
<summary>Create a new repository</summary>
These details <em>will</em> remain <strong>hidden</strong> until expanded.
<pre><code>
git clone git@gitlab.com:orchestra_sc/mllp.git
cd mllp
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
</code></pre>
</details>
</p>

<p>
<details>
<summary>Push an existing folder</summary>
These details <em>will</em> remain <strong>hidden</strong> until expanded.
<pre><code>
git init
git remote add origin git@gitlab.com:orchestra_sc/mllp.git
git add .
git commit -m "Initial commit"
git push -u origin master
</code></pre>
</details>
</p>

<p>
<details>
<summary>Push an existing Git repository</summary>
These details <em>will</em> remain <strong>hidden</strong> until expanded.
<pre><code>
git remote rename origin old-origin
git remote add origin git@gitlab.com:orchestra_sc/mllp.git
git push -u origin --all
git push -u origin --tags
</code></pre>
</details>
</p>

## Orchestra 
[SOAP WIKI](https://de.wikipedia.org/wiki/SOAP)

